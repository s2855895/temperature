package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
/** * Tests the Quoter */
public class TestQuoter {
        @Test
        public void testBook1 ( ) throws Exception{
            Quoter quoter = new Quoter( );
            double price=quoter.getTemperature("1");
            Assertions.assertEquals(33.8 , price, 0.0, "temperature for 1");
        }
}

